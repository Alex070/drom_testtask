package ru.drom.logParser.utils;

import org.apache.commons.cli.*;

public class AnalyzerOptionsCmd {

    public static Options getOptions() {

        Options options = new Options();

        Option u = new Option("u", "level", true, "availability level");
        u.setRequired(true);
        options.addOption(u);

        Option t = new Option("t", "timing", true, "response time");
        t.setRequired(true);
        options.addOption(t);

        return options;
    }

    public static AnalyzerOptions getParseAnalyzerOptions(String[] args) {

        Options options = getOptions();

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
        }
        String levelString = cmd.getOptionValue("level");
        String timingString = cmd.getOptionValue("timing");

        float level = 0;
        float timing = 0;

        try {
            level = Float.parseFloat(levelString);
            timing = Float.parseFloat(timingString);
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        }
        return new AnalyzerOptions(level, timing);
    }
}
