package ru.drom.logParser;

import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogEntry {

    private Date time;
    private Integer httpCode;
    private Float timePeriod;

    public Boolean hasErrorStatus() {
        return httpCode >= 500 && httpCode < 600;
    }
}
