package ru.drom.logParser;

import ru.drom.logParser.utils.AnalyzerOptions;

import java.util.Date;
import java.util.List;

public class EntriesAnalyzer {
    private final float minAccessibilityLevel;
    private final float maxDelay;

    public EntriesAnalyzer(AnalyzerOptions options) {
        minAccessibilityLevel = options.getMinAccessibilityLevel();
        maxDelay = options.getMaxDelay();
    }

    public void analyzeSecondEntries(List<LogEntry> oneSecondEntries) {
        if (oneSecondEntries.isEmpty()) {
            return;
        }
        final long entriesCount = oneSecondEntries.size();
        final long wrongEntriesCount = oneSecondEntries.stream().filter(this::checkIfWrongEntry).count();
        final Date date = oneSecondEntries.get(0).getTime();
        SecondAnalyzeResult secondAnalyzeResult = new SecondAnalyzeResult(entriesCount, wrongEntriesCount, date);
    }

    private Boolean checkIfWrongEntry(LogEntry entry) {
        return entry.hasErrorStatus() || entry.getTimePeriod() > maxDelay;
    }
}
