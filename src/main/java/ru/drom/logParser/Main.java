package ru.drom.logParser;

import ru.drom.logParser.utils.AnalyzerOptions;
import ru.drom.logParser.utils.AnalyzerOptionsCmd;

public class Main {
    static EntriesReader entriesReader = new EntriesReader();

    public static void main(String[] args) {
        AnalyzerOptions analyzerOptions = AnalyzerOptionsCmd.getParseAnalyzerOptions(args);
        EntriesAnalyzer analyzer = new EntriesAnalyzer(analyzerOptions);
    }
}
