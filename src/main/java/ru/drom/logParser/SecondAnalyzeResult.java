package ru.drom.logParser;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
@Getter
public class SecondAnalyzeResult {
    private long entriesCount;
    private long wrongEntriesCount;
    private Date date;

    public float getAccessibilityLevel() {
        return (1f - (wrongEntriesCount / (float) entriesCount)) * 100f;
    }
}
