package ru.drom.logParser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EntryParser {
    static String group1;
    static String group2;
    static String group3;
    static ArrayList<LogEntry> listLogEntry = new ArrayList<LogEntry>();
    static Pattern pattern = Pattern.compile("(\\d{1,3}\\.){3}\\d{1,3} .+? .+? \\[\\d{2}/\\d{2}/\\d{4}:(\\d{2}:\\d{2}:\\d{2})" +
            " \\+\\d{1,4}\\] \".*?\" (\\d{3}) .*? (\\d+?\\.\\d*) \".*?\" \".*?\" .*");

    public static LogEntry getLogEntry(String line) {
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            group1 = matcher.group(1);
            group2 = matcher.group(2);
            group3 = matcher.group(3);

            SimpleDateFormat format = new SimpleDateFormat();
            format.applyPattern("hh:mm:ss");
            try {
                Date time = format.parse(group1);
                Integer httpCode = Integer.parseInt(group2);
                Float timePeriod = Float.parseFloat(group3);
                LogEntry logEntry = new LogEntry(time, httpCode, timePeriod);
                return logEntry;

            } catch (ParseException ex) {
                System.err.printf("Unable to parse log entry string: \"%s\"\n%s%n", line, ex.getMessage());
                return null;
            }
        } else {
            System.err.printf("Unable to parse log entry string: \"%s\"\n%n", line);
            return null;
        }
    }
}
