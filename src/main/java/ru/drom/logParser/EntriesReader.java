package ru.drom.logParser;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Stream;

public class EntriesReader {

    public void readFile(EntriesAnalyzer analyzer) {

        ArrayList<LogEntry> listLogEntry = new ArrayList<>(100);

        File file = new File("C://Users//Alex//Desktop", "access1.log");//TODO переделать
        try (BufferedReader bufer = new BufferedReader(new FileReader(file))) {

            Stream<LogEntry> stream = bufer.lines().map(EntryParser::getLogEntry).filter(entry -> entry != null);
            Date tempTime = null;

            for (LogEntry logEntry : (Iterable<LogEntry>) stream::iterator) {
                if (tempTime == null) {
                    tempTime = logEntry.getTime();
                }
                if (tempTime == logEntry.getTime()) {
                    listLogEntry.add(logEntry);
                } else {
                    tempTime = null;
                    analyzer.analyzeSecondEntries(listLogEntry);
                    listLogEntry.clear();
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
