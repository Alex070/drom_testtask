package ru.drom.logParser.utils;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class AnalyzerOptions {
    private float maxDelay;
    private float minAccessibilityLevel;
}
